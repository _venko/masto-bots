from ananas import PineappleBot, reply, html_strip_tags

class MarcoPoloBot(PineappleBot):
    @reply
    def play(self, status, user):
        toot_content = html_strip_tags(status["content"], linebreaks=True)
        self.log(None, "Mention: {}".format(toot_content))
        if self.is_phrase_marco(toot_content):
            self.log(None, "Sending response to @{}".format(user["acct"]))
            response = "Polo!"
            self.mastodon.status_post(
                "@{}\n{}".format(user["acct"], response),
                in_reply_to_id = status["id"],
                visibility = status["visibility"])

    def is_phrase_marco(self, phrase):
        phrase = phrase.lower()
        phrase = phrase.replace("@marcopolobot", "")
        if "marco" in phrase:
            return True
        return False
