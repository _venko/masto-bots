import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from ananas import PineappleBot, reply, interval

def choices(population, weights=None, *, cum_weights=None, k=1):
    """Return a k sized list of population elements chosen with replacement.
    If the relative weights or cumulative weights are not specified,
    the selections are made with equal probability.
    """
    import itertools as _itertools
    import bisect as _bisect
    from random import random

    if cum_weights is None:
        if weights is None:
            _int = int
            total = len(population)
            return [population[_int(random() * total)] for i in range(k)]
        cum_weights = list(_itertools.accumulate(weights))
    elif weights is not None:
        raise TypeError('Cannot specify both weights and cumulative weights')
    if len(cum_weights) != len(population):
        raise ValueError('The number of weights does not match the population')
    bisect = _bisect.bisect
    total = cum_weights[-1]
    return [population[bisect(cum_weights, random() * total)] for i in range(k)]

def get_machine_json_from_file(machine_location, schema_location):
  with open(schema_location, "r") as schema_file:
    with open(machine_location, "r") as machine_file:
      schema = json.loads(schema_file.read())
      machine = json.loads(machine_file.read())
      validate(machine, schema)
      return machine

class Machine:
  def __init__(self, json):
    self.output = ""
    self.start_state = json["start_state"]
    self.current_state = self.start_state
    self.outputs = dict()
    self.transitions = dict()
    for state in json["states"]:
      label = state["label"]
      transitions = state["transitions"]
      self.outputs[label] = dict([(t["dest"], t["output"]) for t in transitions])
      self.transitions[label] = [(t["dest"], t["probability"]) for t in transitions]
 
  def run_step(self):
    labels, weights = zip(*self.transitions[self.current_state])
    next_state = choices(population=labels, weights=weights)[0]
    self.output += self.outputs[self.current_state][next_state]
    self.current_state = next_state
 
  def run(self):
    while self.transitions[self.current_state] != []:
      self.run_step()

class ScreamBot(PineappleBot):
    def make_scream(self):
        machine = Machine(get_machine_json_from_file("scream_machine.json", "machine.schema"))
        machine.run()
        return machine.output

    def start(self):
        self.scream()

    @interval(30 * 60)
    def scream(self):
        self.log(None, "Screaming.")
        self.mastodon.status_post(self.make_scream())

    @reply
    def respond(self, status, user):
        self.mastodon.status_post(
            "@{}\n{}".format(user['acct'], self.make_scream()),
            in_reply_to_id = status['id'],
            visibility = status['visibility'])
